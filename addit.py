#!/usr/bin/env python

#Some more convenient things

import surface as sf

#quitbutton class
class Quit:
    def __init__(self,text="EXIT"):
        self.qbt = sf.Button(text, lambda wid: sf.gtk.main_quit())
    def getGTK(self):
        return self.qbt.getGTK()
    def show(self):
        self.qbt.show()
    
#Labeled input class
class LabeledInput:
    def __init__(self,text="Input",soi=False):
        self.lbl = sf.Label(text)
        if soi:
            self.inp = sf.Input(text)
        else:
            self.inp = sf.Input("")
        self.box = sf.Box([self.lbl,self.inp],"V")
    def getGTK(self):
        return self.box.getGTK()
    def show(self):
        self.box.show()


if __name__ == "__main__":
    IMG = sf.Window(LabeledInput())
    sf.main()
