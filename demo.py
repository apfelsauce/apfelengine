#!/usr/bin/env python

#DEMO-file for the engine.

import surface as sf
import addit as ad



mBox = sf.Box([],"H",True)
ca=[(sf.Image("image2.png")),(sf.Label("This is an Image"))]

Box1 = sf.Box(ca,"V")
mBox.add(Box1)

label =  sf.Label("TEXT")
inpt  =  sf.Input("TEXT")
btn   = sf.Button("UPDATE")
def call(widget):
    text=inpt.getText()
    label.setText(text)
    btn.setText(text)
btn.clickFunct(call)
cb=[label,inpt,btn]

Box2=sf.Box(cb,"V")
mBox.add(Box2)

slider = sf.Slider("H",0,40,40,1)
label2 = sf.Label("40")
def call2(widget):
    label2.setText(slider.getValue())
slider.addEvnt("changed",call2)
cd=[slider,(label2)]

Box3=sf.Box(cd,"V")
mBox.add((Box3,True,True))

window = sf.Window(mBox,"apfelengine DEMO")

sf.main()
