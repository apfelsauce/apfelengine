#!/usr/bin/env python

#This takes all the GTK Basics and gives them some (subjectively) nicer interfaces.
#addit.py takes those and makes them into even easier to use stuff, that you are likely to need anyways (like the Quit-Button)

import pygtk
pygtk.require("2.0")
import gtk 

#Simple Image class
class Image:
    def __init__(self, filename="apfel.png"):
        self.img = gtk.Image()
        self.img.set_from_file(filename)
    def show(self):
        self.img.show()
    def getGTK(self):
        return self.img

#Complex Image class
class cImage:
    def __init__(self, filename="apfel.png",interp = "NEAREST"):
        #using a pixbuf to be able to scale
        self.pixbuf = gtk.gdk.pixbuf_new_from_file(filename)
        self.img = gtk.Image()
        self.interp = interp
    #set scale
    def scale(self,x,y):
        self.x = x
        self.y = y
        return self
    #change image
    def repPix(self, filename):
        self.pixbuf = gtk.gdk.pixbuf_new_from_file(filename)
    #make image update
    def reset(self):
        if self.interp == "NEAREST":
            interp = gtk.gdk.INTERP_NEAREST
        elif self.interp == "TILES":
            interp = gtk.gdk.INTERP_TILES
        elif self.interp == "BILINEAR":
            interp = gtk.gdk.INTERP_BILINEAR
        elif self.interp == "HYPER":
            interp = gtk.gdk.INTERP_HYPER
        self.img.set_from_pixbuf(self.pixbuf.scale_simple(self.x,self.y,interp))
    def getGTK(self):
        return self.img
    def show(self):
        self.reset()
        self.img.show()
    def getPix(self):
        return self.pixbuf


#Simple Slider class
class Slider:
    def __init__(self,orientation="H",lower=0,upper=10,start=0,step=1,points=0):
        self.adj = gtk.Adjustment(value=start,lower=lower,upper=upper,step_incr=step)
        if orientation == "V" or orientation == "v":
            self.sld = gtk.VScale(adjustment=self.adj)
        else:
            self.sld = gtk.HScale(adjustment=self.adj)
        self.sld.set_digits(points)
    def addEvnt(self,typ,funct):
        self.adj.connect(typ,funct)
    def show(self):
        self.sld.show()
    def getGTK(self):
        return self.sld
    def getValue(self):
        return self.adj.value


#Simple Input class
class Input:
    def __init__(self,text="INPUT"):
        self.inp = gtk.Entry()
        self.inp.set_text(text)
    #get the content of input
    def getText(self):
        return self.inp.get_text()
    def show(self):
        self.inp.show()
    def getGTK(self):
        return self.inp


#Simple Label class
class Label:
    def __init__(self,text="ApfelLabel"):
        self.lbl = gtk.Label(text)
    #change text of label
    def setText(self, text):
        self.lbl.set_text(text)
    def show(self):
        self.lbl.show()
    def getGTK(self):
        return self.lbl


#PlaceholderFunction
def phf(widget):
    print("Apfel")

#Simple dropdown class
class Dropdown:
    def __init__(self,cbf=None,text=None):
        self.cob = gtk.combo_box_new_text()
        if cbf!=None:
            self.changeFunct(cbf)
        if text!=None:
            self.addText(text)
    def changeFunct(self,cbf):
        self.cob.connect("changed", cbf)
    def addText(self, text):
        if type(text)!=type(["a","b"]):
            if type(text)!=type(("a","b")):
                self.cob.append_text(text)
                return True
        for t in text:
            self.cob.append_text(t)
    def show(self):
        self.cob.show()
    def getGTK(self):
        return self.cob
    def getSetting(self):
        return self.cob.get_active_text()

#Simple Button class
class Button:
    def __init__(self,tag="APFEL",cbf=None):
        self.btn = gtk.Button(tag)
        if cbf!=None:
            self.clickFunct(cbf)
    #add callbackfunction
    def clickFunct(self, cbf):
        self.btn.connect("clicked",cbf)
    #change text of button
    def setText(self,text):
        self.btn.set_label(text)
    def show(self):
        self.btn.show()
    def getGTK(self):
        return self.btn



#content for the def-window
ti=[(Label(),True),(Button(),True,True),(Slider(),True,True),(Input(),True,True)]

#An event box class. This can make things clickable, that weren't bevore
class EventBox:
    def __init__(self,stuff=None,cbf=None):
        self.event = gtk.EventBox()
        if stuff!= None:
            self.add(stuff)
        if cbf != None:
            self.clickFunct(cbf)
    #add stuff inside the box
    def add(self,stuff):
        self.stuff = stuff
        self.event.add(self.stuff.getGTK())
    #add callback to click event
    def clickFunct(self,cbf,*data):
        self.event.connect("button_press_event",cbf,data)
    #add callback to key event
    def keyFunct(self, cbf):
        self.event.connect("key_press_event",cbf)
        self.event.connect("key-release-event",cbf)
    def show(self):
        self.stuff.show()
        self.event.show()
    def getGTK(self):
        return self.event



#Simple Box class
class Box:
    def __init__(self,stuff=[],orientation="V",fill=False,gap=0):
        if orientation == "H" or orientation == "h":
            self.box=gtk.HBox(fill,gap)
        else:
            self.box=gtk.VBox(fill,gap)
        self.add(stuff)
    #add a list or tuple of widgets (or just one)
    def add(self,stuff):
        if type(stuff)!=type([1,2]):
            stuff = [stuff]
        for i in stuff:
            n=o=False
            if type(i)==type((1,2)):
                if not len(i)>=3:
                    o = False
                    if len(i)==1:
                        n=False
                        m=i
                    else:
                        n=i[1]
                        m=i[0]
                else:
                    m = i[0]
                    n=i[1]
                    o=i[2]
            else:
                o = False
                n=False
                m=i
            m.show()
            a = m.getGTK()
            self.box.pack_start(a,n,o)
    def show(self):
        self.box.show()
    def getGTK(self):
        return self.box

        
apfel=Box([(Image()),Image("image1.png"),(Box(ti,"V"),True,True)],"H")


class Window:
    def close(self, widget, event, data=None):
        gtk.main_quit()
        return False
    def __init__(self,stuff=None, title="apfelengine",icon="ae-logo.png"):
        #create window with parameters
        self.window=gtk.Window(gtk.WINDOW_TOPLEVEL)
        self.window.connect("delete_event",self.close)
        self.window.set_title(title)
        self.window.set_icon_from_file(icon)
        if stuff !=None:
            self.add(stuff)
    def add(self, stuff):
        #add stuff to the window! (and make it all show up)
        while type(stuff)==type((1,2)) or type(stuff) == type([1,2]):
            stuff=stuff[0]
        stuff.show()
        a = stuff.getGTK()
        self.window.add(a)
        self.window.show()
    def getGTK(self):
        return self.window

def main():
    gtk.main()

#examplething
if __name__ == "__main__":
    IMG = Window()
    main()
